﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecureFlight.Core.Entities
{
    public class Airport
    {
        public Airport()
        {
            this.OriginFlights = new HashSet<Flight>();
            this.DestinationFlights = new HashSet<Flight>();
        }
        public Airport(string code, string name, string city, string country) 
        {
            this.Code = code;
                this.Name = name;
                this.City = city;
                this.Country = country;
               
        }
                public string Code { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public ICollection<Flight> OriginFlights { get; set; }

        public ICollection<Flight> DestinationFlights { get; set; }
    }
}
