using System;
using System.Threading.Tasks;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Core.Services
{
    public class FlightService : IFlightService
    {
        private readonly IFlightRepository _repository;
        public FlightService(IFlightRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        public async Task<OperationResult> AddPassengerToFlightAsync(string personId, long flightId)
        {
            try
            {
                //user/flight validation
                await _repository.AddPassengerToFlightAsync(personId, flightId);
                return new OperationResult(true);
            }
            catch (System.Exception)
            {
                var error = new Error()
                {
                    Code = ErrorCode.InternalError
                };
                //log
                return new OperationResult (error);
            }
        }
    }
}