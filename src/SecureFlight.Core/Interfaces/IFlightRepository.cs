using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IFlightRepository
    {
        Task AddPassengerToFlightAsync(string personId, long flightId);
    }
}