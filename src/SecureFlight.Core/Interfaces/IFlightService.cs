using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IFlightService
    {
         Task<OperationResult> AddPassengerToFlightAsync(string personId, long flightId);
    }
}