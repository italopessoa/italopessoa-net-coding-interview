using System.Threading.Tasks;
using SecureFlight.Core.Interfaces;
using System;
using  SecureFlight.Core.Entities;

namespace SecureFlight.Infrastructure.Repositories
{
    public class FlightRepository : IFlightRepository
    {
        private SecureFlightDbContext _dbContext;

        public FlightRepository(SecureFlightDbContext dbContext)
        {
            _dbContext =  dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task AddPassengerToFlightAsync(string personId, long flightId)
        {
            _dbContext.Add<PassengerFlight>(new PassengerFlight
            {
                FlightId = flightId,
                PassengerId = personId
            });

            await _dbContext.SaveChangesAsync();
        }
    }
}